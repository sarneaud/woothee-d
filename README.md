## `woothee`

A D implementation of [Project Woothee](https://github.com/woothee/woothee), the multi-language HTTP user agent string parser.

### Usage

```d
import woothee;

const agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36";

assert (!isCrawler(agent));

const result = parse(agent);
assert (result.category == Category.pc);
assert (result.name == "Chrome");
assert (result.client_type == ClientType.browser);
assert (result.client_version == "86.0.4240.111");
assert (result.os == "Linux");
assert (result.os_version is null);
assert (result.vendor == "Google");
```

#### `WootheePrecompute`

Regexes and data tables can be precomputed at compile time.  That makes compilation significantly slower, so it's controlled by the `WootheePrecompute` [version flag](https://dlang.org/spec/version.html).  You can enable it in DMD (and compatible compilers) by adding `-version=WootheePrecompute` to the compiler command line.

### Contributing

This implementation was originally translated from [the Rust version](https://github.com/woothee/woothee-rust), but now tracks [the Ruby version](https://github.com/woothee/woothee-ruby) (which is maintained by the overall project maintainer).

This code just follows the Project Woothee spec.  If you want to add support for more user agents, please add it [upstream](https://github.com/woothee/woothee), first.

If you disagree with any parser output, please check if it's different from the Ruby version.  If it is, file a bug report here, otherwise discuss it with the upstream project maintainers.

#### Updating

To update this implementation to a newer version of the Woothee spec, you need to 1) pull in the new Woothee data, and 2) update the code so that tests pass.

Upstream Woothee data is tracked using git submodules.  It's converted from YAML to JSON to avoid adding a YAML parser as a dependency (which may change if Phobos ever includes a YAML parser).  The YAML-to-JSON conversion requires [the remarshal tool](https://github.com/dbohdan/remarshal).

```
git submodule init
git submodule update
cd woothee-data
git tag # to list tags
git checkout vX.X.X
cd ..
./mkjson.sh
# Check all changes in
```

You can run the tests using `dub test`, or using a D compiler directly:

```
dmd -J=data -main -unittest source/woothee.d
./woothee
```

#### Guidelines

A major goal is to keep the D version up-to-date with the Project Woothee spec by making it easy to update by anyone who knows D and can read the Ruby implementation.  

* Treat the Ruby implementation as the definitive version.  It's right, even if you have a different opinion.  Keep the other guidelines in mind, even if you aren't translating directly from the Ruby version
* D and Ruby are different, but please try to keep the structure of the D code similar to the Ruby code
* Keep naming as consistent (or at least predictable) where possible
* If the Ruby code has comments (except for ones that are specific to the Ruby implementation), add them verbatim to the D code
* Boring is good.  The code has a lot of simple logic and string matching because that's how Woothee works.  That's okay.
