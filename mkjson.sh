#!/bin/bash
#
# Converts all the Woothee YAML data files to JSON
#
# Requires remarshal (https://github.com/dbohdan/remarshal)

set -eux

mkdir -p data/woothee/tests

doConversion() {
	remarshal -of json --indent-json 2 --preserve-key-order "$1"
}

doConversion woothee-data/dataset.yaml > data/woothee/dataset.json

for y in woothee-data/testsets/*.yaml
do
	doConversion "$y" > "data/woothee/tests/$(basename "${y/.yaml}").json"
done
