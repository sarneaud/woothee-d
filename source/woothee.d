module woothee;

@safe:

import std.algorithm : among, map, startsWith;
static import std.ascii;
import std.range;
import std.regex;
import std.utf : byCodeUnit;

/**
	The result of parsing a user agent with Woothee

	Member values can be null or unknown.
*/
struct WootheeResult
{
	Category category;
	ClientType client_type;
	string name = null, os = null, os_version = null, client_version = null, vendor = null;
}

/// Detected user agent general category
enum Category
{
	unknown,
	pc,
	smartphone,
	mobilephone,
	appliance,
	crawler,
	misc,
	_ignore, // For testing
}
static assert (Category.init == Category.unknown);

/// Detected user agent client type
enum ClientType
{
	unknown,
	browser,
	os,
	full,
	_ignore, // For testing
}
static assert (ClientType.init == ClientType.unknown);

/**
	Parses a user agent string using Woothee

	The input string may be null or empty or "-".
*/
WootheeResult parse(string agent)
{
	auto parser = WootheeParser(agent);
	parser.parse();
	return parser._result;
}

///
unittest
{
	const agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36";

	assert (!isCrawler(agent));

	const result = parse(agent);
	assert (result.category == Category.pc);
	assert (result.name == "Chrome");
	assert (result.client_type == ClientType.browser);
	assert (result.client_version == "86.0.4240.111");
	assert (result.os == "Linux");
	assert (result.os_version is null);
	assert (result.vendor == "Google");
}

/**
	Quick check for user agents representing crawlers

	It's designed to be faster than a full parse(), while potentially sacrificing accuracy.

	(Added for compatibility with the Ruby version, although the D version of parse() is already fast enough for many applications.)
*/
bool isCrawler(string agent)
{
	if (agent.among(null, "", "-")) return false;
	auto parser = WootheeParser(agent);
	return parser.tryCrawler();
}

private:

immutable(WootheeResult)* lookupDataset(string label) pure
{
	return _dataset.get(label, null);
}

/**
	Returns a 64b ulong with bits set based on the characters in the given string

	Before checking if a "needle" string is contained in a "haystack" string, we can do a quick check if the haystack even has all the characters in the needle, just by ANDing the hashes.

	It works like bloom filter, and allows short-circuiting ~%90 of string searches in a test on real user agents.
*/
ulong bloomHashOf(string s) pure
{
	ulong ret;
	foreach (char c; s)
	{
		ret |= 1UL << (c & 63);
	}
	return ret;
}

struct WootheeParser
{
	WootheeResult _result;
	string _agent;
	ulong _agent_bloom;

	this(string agent)
	{
		_agent = agent;
		_agent_bloom = bloomHashOf(agent);
	}

	void parse()
	{
		if (_agent.among(null, "", "-")) return;
		if (tryCrawler()) return;

		if (tryBrowser())
		{
			tryOS();
			return;
		}

		if (tryMobilePhone()) return;
		if (tryAppliance()) return;
		if (tryMisc()) return;
		// browser unknown. check os only
		if (tryOS()) return;
		tryRareCases();
	}

	void populateWith(ref const(WootheeResult) data)
	{
		if (data.name !is null) _result.name = data.name;
		if (data.category != Category.unknown) _result.category = data.category;
		if (data.os !is null) _result.os = data.os;
		if (data.client_type != ClientType.unknown) _result.client_type = data.client_type;
		if (data.client_version !is null) _result.client_version = data.client_version;
		if (data.vendor !is null) _result.vendor = data.vendor;
	}

	bool populateDataset(string label)
	{
		const data = lookupDataset(label);
		if (data is null) return false;
		populateWith(*data);
		return true;
	}

	bool tryAll(funcs...)()
	{
		static foreach (f; funcs)
		{
			if (f()) return true;
		}
		return false;
	}

	bool contains(string needle)()
	{
		import std.algorithm.searching : canFind;
		enum needle_bloom = bloomHashOf(needle);
		if ((needle_bloom & _agent_bloom) != needle_bloom) return false;
		return _agent.canFind(needle);
	}

	bool tryCrawler()
	{
		return tryAll!(challengeGoogle, challengeCrawlers)();
	}

	bool tryOS()
	{
		return tryAll!(challengeWindows, challengeOSX, challengeLinux, challengeSmartPhone, challengeMobilePhone, challengeAppliance, challengeMiscOS)();
	}

	bool tryBrowser()
	{
		return tryAll!(challengeMSIE, challengeVivaldi, challengeYandexBrowser, challengeSafariChrome, challengeFirefox, challengeOpera, challengeWebview)();
	}

	bool tryMobilePhone()
	{
		return tryAll!(challengeDocomo, challengeAU, challengeSoftBank, challengeWillcom, challengeMiscMobilePhone)();
	}

	bool tryAppliance()
	{
		return tryAll!(challengePlayStation, challengeNintendo, challengeDigitalTV)();
	}

	bool tryMisc()
	{
		return challengeDesktopTools();
	}

	bool tryRareCases()
	{
		return tryAll!(challengeSmartPhonePatterns, challengeSleipnir, challengeHTTPLibrary, challengeMaybeRSSReader, challengeMaybeCrawler)();
	}

	bool challengeGoogle()
	{
		if (!contains!"Google") return false;

		if (contains!"compatible; Googlebot")
		{
			if (contains!"compatible; Googlebot-Mobile") return populateDataset("GoogleBotMobile");
			return populateDataset("GoogleBot");
		}

		if (contains!"Googlebot-Image/") return populateDataset("GoogleBot");
		if (contains!"Mediapartners-Google" && (contains!"compatible; Mediapartners-Google" || _agent == "Mediapartners-Google")) return populateDataset("GoogleMediaPartners");
		if (contains!"Feedfetcher-Google;") return populateDataset("GoogleFeedFetcher");
		if (contains!"AppEngine-Google") return populateDataset("GoogleAppEngine");
		if (contains!"Google Web Preview") return populateDataset("GoogleWebPreview");

		return false;
	}

	bool challengeCrawlers()
	{
		if (contains!"Yahoo" || contains!"help.yahoo.co.jp/help/jp/" || contains!"listing.yahoo.co.jp/support/faq/")
		{
			if (contains!"compatible; Yahoo! Slurp") return populateDataset("YahooSlurp");
			if (contains!"YahooFeedSeekerJp" || contains!"YahooFeedSeekerBetaJp" || contains!"crawler (http://listing.yahoo.co.jp/support/faq/" || contains!"crawler (http://help.yahoo.co.jp/help/jp/" || contains!"Y!J-BRZ/YATSHA crawler" || contains!"Y!J-BRY/YATSH crawler") return populateDataset("YahooJP");
			if (contains!"Yahoo Pipes") return populateDataset("YahooPipes");
		}

		if (contains!"msnbot") return populateDataset("msnbot");
		if (contains!"bingbot" && contains!"compatible; bingbot") return populateDataset("bingbot");
		if (contains!"BingPreview") return populateDataset("BingPreview");
		if (contains!"Baidu" && (contains!"compatible; Baiduspider" || contains!"Baiduspider+" || contains!"Baiduspider-image+")) return populateDataset("Baiduspider");
		if (contains!"Yeti" && (contains!"http://help.naver.com/robots" || contains!"http://help.naver.com/support/robots.html" || contains!"http://naver.me/bot")) return populateDataset("Yeti");
		if (contains!"FeedBurner/") return populateDataset("FeedBurner");
		if (contains!"facebookexternalhit") return populateDataset("facebook");
		if (contains!"Twitterbot/") return populateDataset("twitter");
		if (contains!"ichiro" && (contains!"http://help.goo.ne.jp/door/crawler.html" || contains!"compatible; ichiro/mobile goo;")) return populateDataset("goo");
		if (contains!"gooblogsearch/") return populateDataset("goo");
		if (contains!"Apple-PubSub") return populateDataset("ApplePubSub");
		if (contains!"(www.radian6.com/crawler)") return populateDataset("radian6");
		if (contains!"Genieo/") return populateDataset("Genieo");
		if (contains!"labs.topsy.com/butterfly/") return populateDataset("topsyButterfly");
		if (contains!"rogerbot/1.0 (http://www.seomoz.org/dp/rogerbot") return populateDataset("rogerbot");
		if (contains!"compatible; AhrefsBot/") return populateDataset("AhrefsBot");
		if (contains!"livedoor FeedFetcher" || contains!"Fastladder FeedFetcher") return populateDataset("livedoorFeedFetcher");
		if (contains!"Hatena Antenna" || contains!"Hatena Pagetitle Agent" || contains!"Hatena Diary RSS") return populateDataset("Hatena");
		if (contains!"mixi-check" || contains!"mixi-crawler" || contains!"mixi-news-crawler") return populateDataset("mixi");
		if (contains!"Indy Library" && contains!"compatible; Indy Library") return populateDataset("IndyLibrary");
		if (contains!"trendictionbot") return populateDataset("trendictionbot");

		return false;
	}

	bool challengeMSIE()
	{
		if (!contains!"compatible; MSIE" && !contains!"Trident/" && !contains!"IEMobile") return false;

		static immutable msie_re = buildRegex(`MSIE ([.0-9]+);`);
		static immutable trident_re = buildRegex(`Trident/([.0-9]+);`);
		static immutable trident_version_re = buildRegex(` rv:([.0-9]+)`);
		static immutable iemobile_re = buildRegex(`IEMobile/([.0-9]+);`);

		if (const caps = _agent.matchFirst(msie_re))
		{
			_result.client_version = caps[1];
		}
		else if(_agent.matchFirst(trident_re))
		{
			const caps = _agent.matchFirst(trident_version_re);
			_result.client_version = caps ? caps[1] : null;
		}
		else if(const caps = _agent.matchFirst(iemobile_re))
		{
			_result.client_version = caps[1];
		}
		else
		{
			_result.client_version = null;
		}

		return populateDataset("MSIE");
	}

	bool challengeVivaldi()
	{
		static immutable version_re = buildRegex(`Vivaldi/([.0-9]+)`);
		if (const caps = _agent.matchFirst(version_re))
		{
			_result.client_version = caps[1];
			return populateDataset("Vivaldi");
		}

		return false;
	}

	bool challengeYandexBrowser()
	{
		if (!contains!"YaBrowser/") return false;

		static immutable version_re = buildRegex(`YaBrowser/(\d+\.\d+\.\d+\.\d+)`);

		const caps = _agent.matchFirst(version_re);
		if (caps)
		{
			_result.client_version = caps[1];
			return populateDataset("YaBrowser");
		}

		return false;
	}

	bool challengeSafariChrome()
	{
		if (!contains!"Safari/") return false;
		if (contains!"Chrome" && contains!"wv") return false;

		// Edge
		static immutable edge_version_re = buildRegex(`(?:Edge|Edg|EdgiOS|EdgA)/([.0-9]+)`);
		if (const caps = _agent.matchFirst(edge_version_re))
		{
			_result.client_version = caps[1];
			return populateDataset("Edge");
		}

		static immutable fxios_version_re = buildRegex(`FxiOS/([.0-9]+)`);
		if (const caps = _agent.matchFirst(fxios_version_re))
		{
			_result.client_version = caps[1];
			return populateDataset("Firefox");
		}

		static immutable chrome_version_re = buildRegex(`(?:Chrome|CrMo|CriOS)/([.0-9]+)`);
		if (const caps = _agent.matchFirst(chrome_version_re))
		{
			const chrome_version = caps[1];

			static immutable opera_version_re = buildRegex(`OPR/([.0-9]+)`);
			if (const opera_caps = _agent.matchFirst(opera_version_re))
			{
				if (!populateDataset("Opera")) return false;
				_result.client_version = opera_caps[1];
				return true;
			}

			// Chrome
			if (!populateDataset("Chrome")) return false;
			_result.client_version = chrome_version;
			return true;
		}

		// Google Search App
		if (contains!"GSA")
		{
			static immutable gsa_version_re = buildRegex(`GSA/([.0-9]+)`);
			
			if (const caps = _agent.matchFirst(gsa_version_re))
			{
				_result.client_version = caps[1];
				return populateDataset("GSA");
			}
		}

		// Safari
		if (!populateDataset("Safari")) return false;
		static immutable safari_version_re = buildRegex(`Version/([.0-9]+)`);
		if (const caps = _agent.matchFirst(safari_version_re)) _result.client_version = caps[1];

		return true;
	}

	bool challengeFirefox()
	{
		if (!contains!"Firefox/") return false;
		if (!populateDataset("Firefox")) return false;
		static immutable version_re = buildRegex(`Firefox/([.0-9]+)`);

		if (const caps = _agent.matchFirst(version_re))
		{
			_result.client_version = caps[1];
		}

		return true;
	}

	bool challengeOpera()
	{
		if (!contains!"Opera") return false;

		if (!populateDataset("Opera")) return false;

		static immutable version_re1 = buildRegex(`Version/([.0-9]+)`);
		static immutable version_re2 = buildRegex(`Opera[/ ]([.0-9]+)`);

		if (const caps = _agent.matchFirst(version_re1))
		{
			_result.client_version = caps[1];
		}
		else
		{
			const caps = _agent.matchFirst(version_re2);
			_result.client_version = caps ? caps[1] : null;
		}

		return true;
	}

	bool challengeWebview()
	{
		static immutable version_re = buildRegex(`Version/([.0-9]+)`);

		if (contains!"Chrome" && contains!"wv")
		{
			if (!populateDataset("Webview")) return false;
			const caps = _agent.matchFirst(version_re);
			_result.client_version = caps ? caps[1] : null;
			return true;
		}

		static immutable webview_re = buildRegex(`iP(hone;|ad;|od) .*like Mac OS X`);
		if (_agent.matchFirst(webview_re))
		{
			if (contains!"Safari/") return false;
			if (!populateDataset("Webview")) return false;
			const caps = _agent.matchFirst(version_re);
			_result.client_version = caps ? caps[1] : null;
			return true;
		}

		return false;
	}

	bool challengeDocomo()
	{
		if (!contains!"DoCoMo" && !contains!";FOMA;") return false;
		if (!populateDataset("docomo")) return false;

		static immutable docomo_version_re = buildRegex(`DoCoMo/[.0-9]+[ /]([^- /;()"']+)`);
		static immutable foma_version_re = buildRegex(`\(([^;)]+);FOMA;`);

		if (const caps = _agent.matchFirst(docomo_version_re))
		{
			_result.client_version = caps[1];
		}
		else
		{
			const caps = _agent.matchFirst(foma_version_re);
			_result.client_version = caps ? caps[1] : null;
		}

		return true;
	}

	bool challengeAU()
	{
		if (!contains!"KDDI-") return false;
		if (!populateDataset("au")) return false;
		if (const caps = _agent.matchFirst(_kddi_version_re))
		{
			_result.client_version = caps[1];
		}
		return true;
	}

	bool challengeSoftBank()
	{
		if (!contains!"SoftBank" && !contains!"Vodafone" && !contains!"J-PHONE") return false;
		if (!populateDataset("SoftBank")) return false;
		static immutable version_re = buildRegex(`(?:SoftBank|Vodafone|J-PHONE)/[.0-9]+/([^ /;()]+)`);
		const caps = _agent.matchFirst(version_re);
		_result.client_version = caps ? caps[1] : null;
		return true;
	}

	bool challengeWillcom()
	{
		if (!contains!"WILLCOM" && !contains!"DDIPOCKET") return false;
		if (!populateDataset("willcom")) return false;
		const caps = _agent.matchFirst(_willcom_version_re);
		_result.client_version = caps ? caps[1] : null;
		return true;
	}

	bool challengeMiscMobilePhone()
	{
		if (contains!"jig browser")
		{
			if (!populateDataset("jig")) return false;
			static immutable version_re = buildRegex(`jig browser[^;]+; ([^);]+)`);
			if (const caps = _agent.matchFirst(version_re))
			{
				_result.client_version = caps[1];
			}
			return true;
		}

		if (contains!"emobile/" || contains!"OpenBrowser" || contains!"Browser/Obigo-Browser") return populateDataset("emobile");
		if (contains!"SymbianOS") return populateDataset("SymbianOS");

		if (contains!"Hatena-Mobile-Gateway/")
		{
			if (!populateDataset("MobileTranscoder")) return false;
			_result.client_version = "Hatena";
			return true;
		}

		if (contains!"livedoor-Mobile-Gateway/")
		{
			if (!populateDataset("MobileTranscoder")) return false;
			_result.client_version = "livedoor";
			return true;
		}

		return false;
	}

	bool challengePlayStation()
	{
		string os_version = "";
		immutable(WootheeResult)* data;

		if (contains!"PSP (PlayStation Portable)")
		{
			static immutable psp_os_version_re = buildRegex(`PSP \(PlayStation Portable\); ([.0-9]+)\)`);
			const caps = _agent.matchFirst(psp_os_version_re);
			os_version = caps ? caps[1] : "";
			data = lookupDataset("PSP");
		}
		else if (contains!"PlayStation Vita")
		{
			static immutable pv_os_version_re = buildRegex(`PlayStation Vita ([.0-9]+)\)`);
			const caps = _agent.matchFirst(pv_os_version_re);
			os_version = caps ? caps[1] : "";
			data = lookupDataset("PSVita");
		}
		else if (contains!"PLAYSTATION 3 " || contains!"PLAYSTATION 3;")
		{
			static immutable ps3_os_version_re = buildRegex(`PLAYSTATION 3;? ([.0-9]+)\)`);
			const caps = _agent.matchFirst(ps3_os_version_re);
			os_version = caps ? caps[1] : "";
			data = lookupDataset("PS3");
		}
		else if (contains!"PlayStation 4 ")
		{
			static immutable ps4_os_version_re = buildRegex(`PlayStation 4 ([.0-9]+)\)`);
			const caps = _agent.matchFirst(ps4_os_version_re);
			os_version = caps ? caps[1] : "";
			data = lookupDataset("PS4");
		}
		else
		{
			return false;
		}

		populateWith(*data);

		if (!os_version.empty) _result.os_version = os_version;
		return true;
	}

	bool challengeNintendo()
	{
		if (contains!"Nintendo 3DS;") return populateDataset("Nintendo3DS");
		if (contains!"Nintendo DSi;") return populateDataset("NintendoDSi");
		if (contains!"Nintendo Wii;") return populateDataset("NintendoWii");
		if (contains!"(Nintendo WiiU)") return populateDataset("NintendoWiiU");
		return false;
	}

	bool challengeDigitalTV()
	{
		if (contains!"InettvBrowser/") return populateDataset("DigitalTV");
		return false;
	}

	bool challengeWindows()
	{
		if (!contains!"Windows") return false;

		// Xbox Series
		if (contains!"Xbox")
		{
			if (contains!"Xbox; Xbox One)") return populateDataset("XboxOne");
			// update browser as appliance
			return populateDataset("Xbox360");
		}

		auto win = lookupDataset("Win");
		if (win is null) return false;

		static immutable windows_version_re = buildRegex(`Windows ([ .a-zA-Z0-9]+)[;\\)]`);
		auto caps = _agent.matchFirst(windows_version_re);
		if (!caps)
		{
			// Windows, but version unknown
			_result.category = win.category;
			_result.os = win.name;
			return true;
		}

		string os_version = caps[1];

		switch (os_version)
		{
			case "NT 10.0":
				win = lookupDataset("Win10");
				break;
			case "NT 6.3":
				win = lookupDataset("Win8.1");
				break;
			case "NT 6.2":
				win = lookupDataset("Win8");
				break;
			case "NT 6.1":
				win = lookupDataset("Win7");
				break;
			case "NT 6.0":
				win = lookupDataset("WinVista");
				break;
			case "NT 5.1":
				win = lookupDataset("WinXP");
				break;
			case "NT 5.0":
				win = lookupDataset("Win2000");
				break;
			case "NT 4.0":
				win = lookupDataset("WinNT4");
				break;
			case "98":
				// wow, WinMe is shown as 'Windows 98; Win9x 4.90', fxxxk
				win = lookupDataset("Win98");
				break;
			case "95":
				win = lookupDataset("Win95");
				break;
			case "CE":
				win = lookupDataset("WinCE");
				break;
			default:
				static immutable win_phone_version_re = buildRegex(`^Phone(?: OS)? ([.0-9]+)`);
				caps = os_version.matchFirst(win_phone_version_re);
				if (caps)
				{
					os_version = caps[1];
					win = lookupDataset("WinPhone");
				}
		}

		if (win is null) return false;

		_result.os_version = os_version;
		_result.category = win.category;
		_result.os = win.name;

		return true;
	}

	// OSX PC and iOS devices (strict check)
	bool challengeOSX()
	{
		if (!contains!"Mac OS X") return false;

		static immutable iphone_os_version_re = buildRegex(`; CPU(?: iPhone)? OS (\d+_\d+(?:_\d+)?) like Mac OS X`);
		static immutable os_version_re = buildRegex(`Mac OS X (10[._]\d+(?:[._]\d+)?)(?:\)|;)`);

		auto data = lookupDataset("OSX");
		if (data is null) return false;

		if (contains!"like Mac OS X")
		{
			if (contains!"iPhone;")
			{
				data = lookupDataset("iPhone");
			}
			else if (contains!"iPad;")
			{
				data = lookupDataset("iPad");
			}
			else if (contains!"iPod")
			{
				data = lookupDataset("iPod");
			}
			if (data is null) return false;

			const caps = _agent.matchFirst(iphone_os_version_re);
			if (caps) _result.os_version = caps[1].replace("_", ".");
		}
		else
		{
			const caps = _agent.matchFirst(os_version_re);
			if (caps) _result.os_version = caps[1].replace("_", ".");
		}

		_result.category = data.category;
		_result.os = data.name;
		return true;
	}

	// Linux PC and Android
	bool challengeLinux()
	{
		if (!contains!"Linux") return false;

		immutable(WootheeResult)* data;
		if (contains!"Android")
		{
			static immutable os_version_re = buildRegex(`Android[- ](\d+(?:\.\d+(?:\.\d+)?)?)`);
			data = lookupDataset("Android");
			if (data is null) return false;
			const caps = _agent.matchFirst(os_version_re);
			if (caps) _result.os_version = caps[1];
		}
		else
		{
			data = lookupDataset("Linux");
			if (data is null) return false;
		}

		_result.category = data.category;
		_result.os = data.name;
		return true;
	}

	// all user_agents matches /(iPhone|iPad|iPod|Android|BlackBerry)/
	bool challengeSmartPhone()
	{
		string os_version;

		immutable(WootheeResult)* data;
		if (contains!"iPhone")
		{
			data = lookupDataset("iPhone");
		}
		else if (contains!"iPad")
		{
			data = lookupDataset("iPad");
		}
		else if (contains!"iPod")
		{
			data = lookupDataset("iPod");
		}
		else if (contains!"Android")
		{
			data = lookupDataset("Android");
		}
		else if (contains!"CFNetwork")
		{
			data = lookupDataset("iOS");
		}
		else if (contains!"BB10")
		{
			static immutable bb10_os_version_re = buildRegex(`BB10(?:.+)Version/([.0-9]+) `);
			const caps = _agent.matchFirst(bb10_os_version_re);
			if (caps) os_version = caps[1];
			data = lookupDataset("BlackBerry10");
		}
		else if (contains!"BlackBerry")
		{
			static immutable bb_os_version_re = buildRegex(`BlackBerry(?:\d+)/([.0-9]+) `);
			const caps = _agent.matchFirst(bb_os_version_re);
			if (caps) os_version = caps[1];
			data = lookupDataset("BlackBerry");
		}

		const firefox = lookupDataset("Firefox");
		if (firefox !is null && _result.name == firefox.name)
		{
			// Firefox OS specific pattern
			// http://lawrencemandel.com/2012/07/27/decision-made-firefox-os-user-_agent-string/
			// https://github.com/woothee/woothee/issues/2
			static immutable firefox_os_re = buildRegex(`^Mozilla/[.0-9]+ \((?:Mobile|Tablet);(?:.*;)? rv:([.0-9]+)\) Gecko/[.0-9]+ Firefox/[.0-9]+$`);
			const caps = _agent.matchFirst(firefox_os_re);
			if (caps.length > 1)
			{
				data = lookupDataset("FirefoxOS");
				os_version = caps[1];
			}
		}

		if (data is null) return false;

		_result.category = data.category;
		_result.os = data.name;

		if (!os_version.empty) _result.os_version = os_version;
		return true;
	}

	// mobile phones like KDDI-.*
	bool challengeMobilePhone()
	{
		if (contains!"KDDI-")
		{
			if (const caps = _agent.matchFirst(_kddi_version_re))
			{
				const data = lookupDataset("au");
				if (data is null) return false;
				_result.category = data.category;
				_result.os = data.os;
				_result.client_version = caps[1];
				return true;
			}
		}

		if (contains!"WILLCOM" || contains!"DDIPOCKET")
		{
			
			if (const caps = _agent.matchFirst(_willcom_version_re))
			{
				const data = lookupDataset("willcom");
				if (data is null) return false;
				_result.category = data.category;
				_result.os = data.os;
				_result.client_version = caps[1];
				return true;
			}
		}

		if (contains!"SymbianOS")
		{
			const data = lookupDataset("SymbianOS");
			if (data is null) return false;
			_result.category = data.category;
			_result.os = data.os;
			return true;
		}

		if (contains!"Google Wireless Transcoder")
		{
			if (!populateDataset("MobileTranscoder")) return false;
			_result.client_version = "Google";
		}

		if (contains!"Naver Transcoder")
		{
			if (!populateDataset("MobileTranscoder")) return false;
			_result.client_version = "Naver";
		}

		return false;
	}

	bool challengeDesktopTools()
	{
		if (contains!"AppleSyndication/") return populateDataset("SafariRSSReader");
		if (contains!"compatible; Google Desktop/") return populateDataset("GoogleDesktop");
		if (contains!"Windows-RSS-Platform") return populateDataset("WindowsRSSReader");
		return false;
	}

	bool challengeSmartPhonePatterns()
	{
		if (contains!"CFNetwork/")
		{
			// This is like iPhone, but only Category and Name are filled
			const data = lookupDataset("iOS");
			if (data is null) return false;
			_result.os = data.name;
			_result.category = data.category;
			return true;
		}
		return false;
	}

	bool challengeSleipnir()
	{
		if (!contains!"Sleipnir/") return false;
		const win = lookupDataset("Win");
		if (win is null) return false;
		populateDataset("Sleipnir");

		static immutable version_re = buildRegex(`Sleipnir/([.0-9]+)`);
		const caps = _agent.matchFirst(version_re);
		_result.client_version = caps ? caps[1] : null;
		_result.category = win.category;
		_result.os = win.name;
		return true;
	}

	bool challengeHTTPLibrary()
	{
		string client_version;

		static immutable php_re = buildRegex(`^(?:PHP|WordPress|CakePHP|PukiWiki|PECL::HTTP)(?:/| |$)`);
		static immutable pear_re = buildRegex(`(?:PEAR HTTP_Request|HTTP_Request)(?: class|2)`);
		static immutable http_client_re = buildRegex(`^(?:Apache-HttpClient/|Jakarta Commons-HttpClient/|Java/)`);
		static immutable http_client_other_re = buildRegex(`[- ]HttpClient(/|$)`);

		if (_agent.matchFirst(http_client_re) || _agent.matchFirst(http_client_other_re) || contains!"Java(TM) 2 Runtime Environment,")
		{
			client_version = "Java";
		}
		else if (_agent.startsWith("Wget/"))
		{
			client_version = "wget";
		}
		else if (_agent.startsWith("libwww-perl") || _agent.startsWith("WWW-Mechanize") || _agent.startsWith("LWP::Simple") || _agent.startsWith("LWP ") || _agent.startsWith("lwp-trivial"))
		{
			client_version = "perl";
		}
		else if (_agent.startsWith("Ruby") || _agent.startsWith("feedzirra") || _agent.startsWith("Typhoeus"))
		{
			client_version = "ruby";
		}
		else if (_agent.startsWith("Python-urllib/") || _agent.startsWith("Twisted "))
		{
			client_version = "python";
		}
		else if (_agent.matchFirst(php_re) || _agent.matchFirst(pear_re))
		{
			client_version = "php";
		}
		else if (_agent.startsWith("curl/"))
		{
			client_version = "curl";
		}
		else
		{
			return false;
		}

		if (!populateDataset("HTTPLibrary")) return false;
		_result.client_version = client_version;
		return true;
	}

	bool challengeMaybeRSSReader()
	{
		import std.algorithm.searching : canFind;
		static immutable maybe_rss_re = buildRegex(`(?i)rss(?:reader|bar|[-_ /;()]|[ +]*/)`);
		if (_agent.matchFirst(maybe_rss_re) || contains!"cococ/" || _agent.map!(std.ascii.toLower).canFind("headline-reader"))
		{
			return populateDataset("VariousRSSReader");
		}
		return false;
	}

	bool challengeMaybeCrawler()
	{
		static immutable maybe_crawler_re = buildRegex(`(?i)(?:bot|crawler|spider)(?:[-_ ./;@()]|$)`);
		static immutable maybe_crawler_other_re = buildRegex(`(?:Rome Client |UnwindFetchor/|ia_archiver |Summify |PostRank/)`);
		static immutable maybe_feed_parser_re = buildRegex(`(?i)(?:feed|web) ?parse`);
		static immutable maybe_watchdog_re = buildRegex(`(?i)watch ?dog`);
		if (_agent.matchFirst(maybe_crawler_re) || _agent.matchFirst(maybe_crawler_other_re) || contains!"ASP-Ranker Feed Crawler" || _agent.matchFirst(maybe_feed_parser_re) || _agent.matchFirst(maybe_watchdog_re))
		{
			return populateDataset("VariousCrawler");
		}
		return false;
	}

	// Nintendo DSi/Wii with Opera
	bool challengeAppliance()
	{
		if (contains!"Nintendo DSi;")
		{
			const data = lookupDataset("NintendoDSi");
			if (data is null) return false;
			_result.category = data.category;
			_result.os = data.os;
			return true;
		}

		if (contains!"Nintendo Wii;")
		{
			const data = lookupDataset("NintendoWii");
			if (data is null) return false;
			_result.category = data.category;
			_result.os = data.os;
			return true;
		}

		return false;
	}

	// Win98, BSD, classic MacOS, ...
	bool challengeMiscOS()
	{
		immutable(WootheeResult)* data;
		if (contains!"(Win98;")
		{
			_result.os_version = "98";
			data = lookupDataset("Win98");
		}
		else if (contains!"Macintosh; U; PPC;" || contains!"Mac_PowerPC")
		{
			static immutable ppc_os_version_re = buildRegex(`rv:(\d+\.\d+\.\d+)`);
			const caps = _agent.matchFirst(ppc_os_version_re);
			if (caps) _result.os_version = caps[1];
			data = lookupDataset("MacOS");
		}
		else if (contains!"X11; FreeBSD ")
		{
			static immutable fbsd_os_version_re = buildRegex(`FreeBSD ([^;\)]+);`);
			if (const caps = _agent.matchFirst(fbsd_os_version_re)) _result.os_version = caps[1];
			data = lookupDataset("BSD");
		}
		else if (contains!"X11; CrOS ")
		{
			static immutable cros_os_version_re = buildRegex(`CrOS ([^\)]+)\)`);
			if (const caps = _agent.matchFirst(cros_os_version_re)) _result.os_version = caps[1];
			data = lookupDataset("ChromeOS");
		}

		if (data is null) return false;
		_result.category = data.category;
		_result.os = data.name;
		return true;
	}
}

struct CaseData
{
	string tag;
	const(WootheeResult)* data;
}

immutable(WootheeResult*[string]) _dataset;
shared static this() @trusted
{
	version(WootheePrecompute)
	{
		static immutable case_data = parseData(import("woothee/dataset.json"));
	}
	else
	{
		immutable case_data = parseData(import("woothee/dataset.json"));
	}

	foreach (d; case_data)
	{
		_dataset[d.tag] = d.data;
	}
}
version(WootheePrecompute)
{
	auto buildRegex(string r)
	{
		return regex(r);
	}
}
else
{
	auto buildRegex(string r)
	{
		return r;
	}
}

@trusted // FIXME: std.json isn't @safe-compatible
immutable(CaseData[]) parseData(string source)
{
	import std.json;
	const objects = parseJSON(source).array;

	Category[string] categories = ["UNKNOWN": Category.unknown];
	foreach (l; __traits(allMembers, Category)) categories[l] = __traits(getMember, Category, l);
	ClientType[string] client_types = ["UNKNOWN": ClientType.unknown];
	foreach (l; __traits(allMembers, ClientType)) client_types[l] = __traits(getMember, ClientType, l);

	immutable(CaseData) processObj(JSONValue obj)
	{
		string label;
		// Some test cases explicitly require certain result members to be "unknown".
		// By default, the test case expresses no opinion, and the relevant member should just be ignored.
		// The test cases use the special string "UNKNOWN" for explicitly unknown members.
		// Unitialised (null) strings get the ignore handling.
		WootheeResult result = {
			category: Category._ignore,
			client_type: ClientType._ignore,
		};
		// Can't use byKeyValue because of this:
		// https://issues.dlang.org/show_bug.cgi?id=14151
		foreach (key; obj.object.keys)
		{
			auto value = obj.object[key];
			switch (key)
			{
				case "label":
				case "target":
					label = value.isNull ? null : value.str;
					break;
				case "category":
					result.category = categories[value.str];
					break;
				case "name":
					result.name = value.str;
					break;
				case "os":
					result.os = value.str;
					break;
				case "os_version":
					result.os_version = value.str;
					break;
				case "type":
					result.client_type = client_types[value.str];
					break;
				case "vendor":
					result.vendor = value.str;
					break;
				case "version":
					result.client_version = value.str;
					break;
				default:
					assert (false, "Unhandled data key: " ~ key);
			}
		}
		return immutable(CaseData)(label, new immutable(WootheeResult)(result.tupleof));
	}

	return objects.map!processObj.array;
}

unittest
{
	bool runTest(string test_path)
	{
		import std.file : readText;
		const src = readText(test_path);

		bool has_written_path = false;
		bool is_all_passed = true;
		foreach (d; parseData(src))
		{
			const target = d.tag;
			const ex_result = d.data;
			const result = parse(target);

			bool isStrMatch(string expected, string actual)
			{
				if (expected is null) return true;
				if (expected == "UNKNOWN") return actual is null;
				return expected == actual;
			}

			bool is_pass = true;
			is_pass = is_pass && ex_result.category.among(Category._ignore, result.category);
			is_pass = is_pass && ex_result.client_type.among(ClientType._ignore, result.client_type);
			is_pass = is_pass && isStrMatch(ex_result.name, result.name);
			is_pass = is_pass && isStrMatch(ex_result.os, result.os);
			is_pass = is_pass && isStrMatch(ex_result.os_version, result.os_version);
			is_pass = is_pass && isStrMatch(ex_result.client_version, result.client_version);
			is_pass = is_pass && isStrMatch(ex_result.vendor, result.vendor);

			if (!is_pass)
			{
				import std.stdio : stderr;
				// The way std.stdio.stderr initialises its global singleton isn't currently @safe
				auto output = (() @trusted => stderr)();
				if (!has_written_path)
				{
					output.writeln("*** From ", test_path);
					has_written_path = true;
				}
				output.writeln("---\n", target, '\n', result, '\n', *ex_result);
			}

			is_all_passed &= is_pass;
		}
		return is_all_passed;
	}

	import std.path : buildPath;
	import std.file : dirEntries, SpanMode;
	const test_data_path = buildPath("data", "woothee", "tests");
	const test_filenames = (() @trusted => dirEntries(test_data_path, SpanMode.shallow).array)();
	bool is_pass = true;
	foreach (test_filename; test_filenames)
	{
		is_pass &= runTest(test_filename);
	}
	assert (is_pass);
}

immutable _kddi_version_re = buildRegex(`KDDI-([^- /;()"']+)`);
immutable _willcom_version_re = buildRegex(`(?:WILLCOM|DDIPOCKET);[^/]+/([^ /;()]+)`);
